# moxlib

A collection of data structures and helper functions that are shared between various
projects of [Moxxy](https://codeberg.org/moxxy).

Note that this library is not intended for use outside of those projects.
