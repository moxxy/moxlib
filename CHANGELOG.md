## 0.1.0

* Add [AwaitableDataSender]

## 0.1.1

* Switch to selfhosted pub repository

## 0.1.2

* Add [DeterministicFiniteAutomaton] and [MealyAutomaton]

## 0.1.3

* Add [implies] from the Moxxy main repository
